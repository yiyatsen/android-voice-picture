package com.go_fun.photos.manager.ORMLite;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created by Phil on 2017/11/22.
 */

@Database(entities = {PhotoDbInfo.class}, version = 1, exportSchema = false)
public abstract  class PhotoInfoDatabase  extends RoomDatabase {
    public abstract PhotoInfoDao getPhotoInfoDao();
}