package com.go_fun.photos.manager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.integration.okhttp.OkHttpUrlLoader;
import com.bumptech.glide.load.model.GlideUrl;
import com.go_fun.photos.manager.ORMLite.Db;
import com.go_fun.photos.manager.ORMLite.PhotoDbInfo;
import com.go_fun.photos.manager.ORMLite.PhotoInfoDao;
import com.go_fun.photos.manager.ORMLite.PhotoInfoDatabase;
import com.go_fun.photos.manager.PhotosListView.Photo;
import com.go_fun.photos.manager.PhotosListView.RecyclerViewFragment;
import com.go_fun.photos.manager.Util.FileAccess;
import com.go_fun.photos.manager.Util.Listeners;
import com.squareup.okhttp.OkHttpClient;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Locale;


public class PhotosList extends AppCompatActivity {
    public static final int REQUEST_FORM = 10001;
    public static final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 255;
    private RecyclerViewFragment recyclerViewFragment;
    private Listeners.ButtonEffectOnTouchListener buttonEffectOnTouchListener = new Listeners.ButtonEffectOnTouchListener();
    EditText edtTitle;
    ImageView ivRefresh;
    ImageView ivMic;

    private static final String TAG = "tag_info";
    String[] permission = {Manifest.permission.WRITE_EXTERNAL_STORAGE};

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photos_list);

        int hasPermission = ContextCompat.checkSelfPermission(PhotosList.this , Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (hasPermission != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(PhotosList.this, permission, WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
        }
        ivRefresh = findViewById(R.id.imgRefresh);
        ivMic = findViewById(R.id.ivMic);
        edtTitle = findViewById(R.id.edt_title);

        ivRefresh.setOnTouchListener(buttonEffectOnTouchListener);
        ivMic.setOnTouchListener(buttonEffectOnTouchListener);

        Glide.get(this).register(GlideUrl.class, InputStream.class, new OkHttpUrlLoader.Factory(new OkHttpClient()));

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                readDatabase();
            }
        });
        t.start();
        try { t.join(); } catch (InterruptedException e) { e.printStackTrace(); }


        FileAccess.RetrievingImages(this);
        copyPhotoDbInfoToList();
        FileAccess.FilterMatchedImages();

//        Photo photo;
//        PhotoDbInfo photoDbInfo;

        if (savedInstanceState == null) {
        }

        recyclerViewFragment = new RecyclerViewFragment();
        replaceFragment(recyclerViewFragment);

        edtTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                FileAccess.FilterMatchedImages(edtTitle.getText().toString());

                recyclerViewFragment = new RecyclerViewFragment();
                replaceFragment(recyclerViewFragment);

//                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//                transaction.remove(recyclerViewFragment);
//                recyclerViewFragment = new RecyclerViewFragment();
//                transaction.attach(recyclerViewFragment);
//                transaction.commit();
            }
        });
    }

    private void copyPhotoDbInfoToList() {
        Photo photo;
        PhotoDbInfo photoDbInfo;
        for(int i = 0; i < FileAccess.lstAPhoto.size(); i++) {
            photo = FileAccess.lstAPhoto.get(i);

            for (int j = 0; j < Db.lstPhotoInfo.size(); j++) {
                photoDbInfo = Db.lstPhotoInfo.get(j);
                if (photoDbInfo.imagePath.equals(photo.getImagePath())) {
                    photo.setTitle(photoDbInfo.title);
                    FileAccess.lstAPhoto.set(i, photo);
                    break;
                }
            }
        }
    }

    private void readDatabase() {
        PhotoInfoDatabase photoInfoDatabase = Room.databaseBuilder(this, PhotoInfoDatabase.class, "photos").build();
        PhotoInfoDao dao = photoInfoDatabase.getPhotoInfoDao();
        Db.readDatabase(dao);
    }

    private void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.commit();
    }

    public void getSpeechInput(View view) {

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Listening...");

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 10);
        } else {
            Toast.makeText(this, R.string.device_not_speech_input, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Toast.makeText(this, "requestCode="+requestCode+",resultCode="+resultCode, Toast.LENGTH_SHORT).show();
        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String text = edtTitle.getText().toString();
                    int sel_start = edtTitle.getSelectionStart();
                    int sel_end = edtTitle.getSelectionEnd();
                    String speak_text = result.get(0);

                    edtTitle.setText(text.substring(0, sel_start) + speak_text + text.substring(sel_end, text.length()));
                    edtTitle.setSelection(edtTitle.length());
                }
                break;
            case REQUEST_FORM:
                if(resultCode == RESULT_OK) {
                    refreshList(ivRefresh);
                }
                break;
        }
    }
    
    public void refreshList(View view) {
        recyclerViewFragment.refreshList();
    }

    public void onClickBack(View view) {
        finish();
    }
}
