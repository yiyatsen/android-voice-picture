package com.go_fun.photos.manager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.integration.okhttp.OkHttpUrlLoader;
import com.bumptech.glide.load.model.GlideUrl;
import com.go_fun.photos.manager.ORMLite.Db;
import com.go_fun.photos.manager.ORMLite.PhotoInfoDao;
import com.go_fun.photos.manager.ORMLite.PhotoInfoDatabase;
import com.go_fun.photos.manager.Util.Listeners;
import com.squareup.okhttp.OkHttpClient;

import java.io.InputStream;

//import android.content.ContentResolver;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private final static String URI = "http://www.krisyaoartech.com/";
    public static final int REQUEST_FORM = 10001;
    public static final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 255;
    public static final int READ_EXTERNAL_STORAGE_REQUEST_CODE = 254;
    private Listeners.ButtonEffectOnTouchListener buttonEffectOnTouchListener = new Listeners.ButtonEffectOnTouchListener();
    ImageView ivGallery;
    ImageView ivCamera;
    ImageView imageSponsor;

    String[] permission = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ivGallery = findViewById(R.id.enterGallery);
        ivCamera = findViewById(R.id.enterCamera);
        imageSponsor = findViewById(R.id.imageSponsor);

        ivGallery.setOnTouchListener(buttonEffectOnTouchListener);
        ivCamera.setOnTouchListener(buttonEffectOnTouchListener);
        imageSponsor.setOnTouchListener(buttonEffectOnTouchListener);

        Glide.get(this).register(GlideUrl.class, InputStream.class, new OkHttpUrlLoader.Factory(new OkHttpClient()));

        //readDatabaseWithThreade();

//        hideSystemUI();
    }


    @Override
    protected void onResume() {
        super.onResume();

        int hasPermission = ContextCompat.checkSelfPermission(MainActivity.this , Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (hasPermission != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this, permission, WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
        }
        /*hasPermission = ContextCompat.checkSelfPermission(MainActivity.this , Manifest.permission.READ_EXTERNAL_STORAGE);
        if (hasPermission != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this, permission, READ_EXTERNAL_STORAGE_REQUEST_CODE);
        }*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case READ_EXTERNAL_STORAGE_REQUEST_CODE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Toast.makeText(this, "READ PERMISSION_GRANTED", Toast.LENGTH_LONG).show();
                } else {
                    finish();
                }
            case WRITE_EXTERNAL_STORAGE_REQUEST_CODE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Toast.makeText(this, "WRITE PERMISSION_GRANTED", Toast.LENGTH_LONG).show();
                } else {
                    finish();
                }
                return;
        }
    }

    private void readDatabaseWithThreade() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                readDatabase();
            }
        });
        t.start();
        try { t.join(); } catch (InterruptedException e) { e.printStackTrace(); }
    }

    private void readDatabase() {
        PhotoInfoDatabase photoInfoDatabase = Room.databaseBuilder(this, PhotoInfoDatabase.class, "photos").build();
        PhotoInfoDao dao = photoInfoDatabase.getPhotoInfoDao();
        Db.readDatabase(dao);
    }

    public void onClickSponsor(View view) {
        Intent browse = new Intent( Intent.ACTION_VIEW , Uri.parse(URI));
        startActivity( browse );
    }

    public void onClickEnterCamera(View view) {
        Intent intent = new Intent(this, CameraActivity.class);
        startActivity(intent);
    }

    public void onClickEnterGallery(View view) {
        Intent intent = new Intent(this, PhotosList.class);
        startActivity(intent);
    }

    public void onClickBack(View view) {
        finish();
    }


}
