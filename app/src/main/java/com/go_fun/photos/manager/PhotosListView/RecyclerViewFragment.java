package com.go_fun.photos.manager.PhotosListView;

//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.media.ThumbnailUtils;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.go_fun.photos.manager.ORMLite.Db;
import com.go_fun.photos.manager.ORMLite.PhotoDbInfo;
import com.go_fun.photos.manager.R;
import com.go_fun.photos.manager.Util.FileAccess;

import java.util.ArrayList;
import java.util.List;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection;

import static android.support.constraint.Constraints.TAG;
import static com.go_fun.photos.manager.MainActivity.REQUEST_FORM;

//import android.widget.Button;
//import android.widget.Toast;
//import java.util.Arrays;
//import java.util.stream.Collector;
//import java.util.stream.Collectors;

public class RecyclerViewFragment extends Fragment {
    private static final int COLUMN_PER_ROW  = 4;
    private SectionedRecyclerViewAdapter sectionAdapter;
    private String clickHeaderTitle;
    private int clickPosition;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view, container, false);

        sectionAdapter = new SectionedRecyclerViewAdapter();
        int date_count = FileAccess.lstSDate.size();
        for (int i = 0; i < date_count ; i++) {
            String header_date = FileAccess.lstSDate.get(i);
            sectionAdapter.addSection(header_date, new PhotoSection(header_date, getPhotosList(header_date)));
        }

        RecyclerView recyclerView = view.findViewById(R.id.recyclerview);

        GridLayoutManager glm = new GridLayoutManager(getContext(), COLUMN_PER_ROW);
        glm.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (sectionAdapter.getSectionItemViewType(position)) {
                    case SectionedRecyclerViewAdapter.VIEW_TYPE_HEADER:
                        return COLUMN_PER_ROW;
                    default:
                        return 1;
                }
            }
        });
        recyclerView.setLayoutManager(glm);
        recyclerView.setAdapter(sectionAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() instanceof AppCompatActivity) {
            AppCompatActivity activity = ((AppCompatActivity) getActivity());
            if (activity.getSupportActionBar() != null) {
                activity.getSupportActionBar().setTitle(R.string.app_name);
            }
        }
    }

    public boolean dataMatch(final List<Photo> list, final String date){
        return list.equals(date);
    }

    private List<Photo> getPhotosList(final String headerDate) {
        List<Photo> PhotoList = new ArrayList<>();

        for (Photo p: FileAccess.lstSPhoto) {
            if(p.getDateAdded().equals(headerDate)) {
                PhotoList.add(p);
            }
        }

        return PhotoList;
    }

    private class PhotoSection extends StatelessSection {

        String title;
        List<Photo> list;

        PhotoSection(String title, List<Photo> list) {
            super(SectionParameters.builder()
                    .itemResourceId(R.layout.cardveiw_item)
                    .headerResourceId(R.layout.cardview_header)
                    .build());

            this.title = title;
            this.list = list;
        }

        @Override
        public int getContentItemsTotal() {
            return list.size();
        }

        @Override
        public RecyclerView.ViewHolder getItemViewHolder(View view) {
            return new ItemViewHolder(view);
        }

        @Override
        public void onBindItemViewHolder(RecyclerView.ViewHolder holder, int position) {
            final int pos = position;
            final ItemViewHolder itemHolder = (ItemViewHolder) holder;

            final Photo photo = list.get(position);
            final int imageIndex = photo.getInex();
            String title = photo.getTitle();

            //Log.v(TAG, "setOnClickListener: photo.getImagePath()=" + photo.getImagePath() );
            if(Db.photoInfoInDb(photo.getImagePath())) {
                for(PhotoDbInfo i : Db.lstPhotoInfo) {
                    Log.v(TAG, "setOnClickListener: i.imagePath=" + i.imagePath );
                    if(i.imagePath.equals(photo.getImagePath())) {
                        //itemHolder.HeaderTitle = i.addedDate;
                        title = i.title;
                        break;
                    }
                }
            }
            final String HeaderTitle = photo.getDateAdded();
            //Log.v(TAG, "setOnClickListener: HeaderTitle=" + HeaderTitle );
            //Log.v(TAG, "setOnClickListener: title=" + title );

            Glide.with(getContext()).load(photo.getImagePath()).thumbnail(0.1f)
                    .into(itemHolder.imageView);

            //Log.v(TAG, "onBindItemViewHolder: position=" +position + " title=" +title);
            itemHolder.tvItem.setText(title);
            //Log.i(TAG, "onBindItemViewHolder: title="+title);

            itemHolder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickHeaderTitle = HeaderTitle;
                    clickPosition = pos;

                    //Log.v(TAG, "setOnClickListener: clickHeaderTitle=" + clickHeaderTitle + " clickPosition=" + clickPosition);
                    Intent intent = new Intent();
                    intent.setClass( getContext(), SinglePhotoActivity.class);
                    intent.putExtra("IMAGE_INDEX", imageIndex);

                    getActivity().startActivityForResult(intent, REQUEST_FORM);
                }
            });
        }

        @Override
        public RecyclerView.ViewHolder getHeaderViewHolder(View view) {
            return new HeaderViewHolder(view);
        }

        @Override
        public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder) {
            HeaderViewHolder headerHolder = (HeaderViewHolder) holder;

            headerHolder.tvTitle.setText(title);

            /*headerHolder.btnMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(), String.format("Clicked on more button from the header of Section %s",
                            title),
                            Toast.LENGTH_SHORT).show();
                }
            });*/
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvTitle;

        HeaderViewHolder(View view) {
            super(view);

            tvTitle = view.findViewById(R.id.photo_title_id);
        }
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {

        private final View rootView;
        private final ImageView imageView;
        private final TextView tvItem;
        //private String HeaderTitle;

        ItemViewHolder(View view) {
            super(view);

            rootView = view;
            imageView = view.findViewById(R.id.photo_img_id);
            tvItem = view.findViewById(R.id.photo_title_id);
            //tvSubItem = (TextView) view.findViewById(R.id.tvSubItem);
        }
    }

    public void refreshList() {
        //Log.v(TAG, "refreshList: clickHeaderTitle=" + clickHeaderTitle + " clickPosition=" + clickPosition);
        //PhotoSection photoSection = (PhotoSection)sectionAdapter.getSection(clickHeaderTitle);

        sectionAdapter.notifyItemChangedInSection(clickHeaderTitle, clickPosition);
    }

    public void refreshWholeList() {
        //sectionAdapter.removeAllSections();
        sectionAdapter.notifyDataSetChanged();
        //sectionAdapter.notify();
    }
}
