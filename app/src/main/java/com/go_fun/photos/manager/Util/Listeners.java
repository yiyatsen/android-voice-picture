package com.go_fun.photos.manager.Util;

import android.graphics.PorterDuff;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class Listeners {
    public static class ButtonEffectOnTouchListener implements View.OnTouchListener {
        public boolean onTouch(View v, MotionEvent event) {
            ImageView view = (ImageView) v;
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                view.getDrawable().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
            } else {
                view.getDrawable().clearColorFilter();
            }
            view.invalidate();

            return false;
        }
    }
}
