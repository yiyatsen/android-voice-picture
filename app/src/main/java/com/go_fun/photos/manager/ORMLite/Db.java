package com.go_fun.photos.manager.ORMLite;

import java.util.LinkedList;
import java.util.List;

public class Db {
    static public List<PhotoDbInfo> lstPhotoInfo = new LinkedList<>();

    static public boolean photoTitleMatchDb(String text) {
        for (PhotoDbInfo i : lstPhotoInfo) {
            if(i.title.indexOf(text) >= 0)
                return true;
        }
        return false;
    }

    static public boolean photoInfoInDb(String imagePath) {
        for (PhotoDbInfo i : lstPhotoInfo) {
            if(i.imagePath.equals(imagePath))
                return true;
        }
        return false;
    }

    static public void readDatabase(PhotoInfoDao dao) {
        lstPhotoInfo = dao.getAllPhotoInfo();
    }

    static public void deleteAllUser(PhotoInfoDao dao) {
//        //Log.i(TAG, "删除数据库...");
//        List<PhotoDbInfo> users = dao.getAllUPhotoInfos();
//        for (User u : users) {
//            //Log.i(TAG, "删除: " + u.id + "," + u.name + "," + u.age + "," + u.updateTime);
////            User photo = new User();
////            u.id = photo.id;
//            dao.deleteUser(u);
//        }
//
//        //Log.i(TAG, "删除数据库完毕.");
    }

    static public void add(PhotoInfoDao dao, String imagePath, String title, String addedDate, String addedTime) {
        PhotoDbInfo photo = new PhotoDbInfo();
        photo.imagePath = imagePath;
        photo.title = title;
        photo.addedDate = addedDate;
        photo.addedTime = addedTime;

        dao.insertPhotoInfo(photo);
    }

    static public void updateUser(PhotoInfoDao dao, String imagePath, String title, String addedDate, String addedTime) {
        PhotoDbInfo photo = new PhotoDbInfo();
        photo.imagePath = imagePath;
        photo.title = title;
        photo.addedDate = addedDate;
        photo.addedTime = addedTime;

        dao.updatePhotoInfo(photo);
    }

    static public void deleteUser(PhotoInfoDao dao, String imagePath) {
        PhotoDbInfo photo = new PhotoDbInfo();
        photo.imagePath = imagePath;
        dao.deletePhotoInfo(photo);
    }
}
