package com.go_fun.photos.manager;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class CameraActivity extends AppCompatActivity implements CameraInterface {

    @Override
    public void openPhotoFileSave(String photoFile) {
        Intent intent = new Intent(this, PhotoSaveActivity.class);
        intent.putExtra("PHOTO_FILE", photoFile);
        startActivity(intent);
        finish();
    }

    @Override
    public void goGallery() {
        //backFromGallery = true;
        startActivity(new Intent(this, PhotosList.class));
        finish();
    }

    private static final String TAG = "CameraActivity";
    final int CAMERA_PERMISSION_REQUEST_CODE = 253;
    String[] permission = {Manifest.permission.CAMERA};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Log.i(TAG, "CameraActivity->onCreate: ");
        super.onCreate(savedInstanceState);

//        int hasPermission = ContextCompat.checkSelfPermission(CameraActivity.this , Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        if (hasPermission != PackageManager.PERMISSION_GRANTED){
//            ActivityCompat.requestPermissions(CameraActivity.this, permission, CAMERA_PERMISSION_REQUEST_CODE);
//        }

        setContentView(R.layout.activity_camera);
        if (null == savedInstanceState) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                    && ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, permission, CAMERA_PERMISSION_REQUEST_CODE);
            }
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, CameraFragment.newInstance())
                    .commit();

        }
    }

    public void onClickBack(View view) {
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        //Log.i(TAG, "onRequestPermissionsResult: requestCode="+requestCode);
        switch (requestCode) {
            case CAMERA_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    finish();
                }
                return;
            }
        }
    }

}
