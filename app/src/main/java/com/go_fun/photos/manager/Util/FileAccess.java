package com.go_fun.photos.manager.Util;

import android.content.Context;
import android.database.Cursor;
import android.hardware.camera2.CameraCharacteristics;
import android.provider.MediaStore;

import com.go_fun.photos.manager.PhotosListView.Photo;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class FileAccess {
    static public Integer lensFacing = CameraCharacteristics.LENS_FACING_BACK;
    static public List<String> lstADate = new ArrayList<>();
    static public List<Photo> lstAPhoto = new ArrayList<>();
    static public List<String> lstSDate = new ArrayList<>();
    static public List<Photo> lstSPhoto = new ArrayList<>();

    static public void FilterMatchedImages() {
        FilterMatchedImages("");
    }

    static public void FilterMatchedImages(String text) {
        lstSDate = new ArrayList<>() ;
        lstSPhoto = new ArrayList<>();

        if(!text.equals("")) {
            for (Photo p : lstAPhoto) {
                if(textCompare(p.getTitle(), text)) {
                    lstSPhoto.add(p);
                }
            }

            String header_date = "";
            for (Photo p : lstSPhoto) {
                if(!header_date.equals(p.getDateAdded())) {
                    lstSDate.add(p.getDateAdded());
                    header_date = p.getDateAdded();
                }
            }
        } else {
            lstSDate.addAll(lstADate);
            lstSPhoto.addAll(lstAPhoto);
        }
        Collections.reverse(lstSDate);
        Collections.reverse(lstSPhoto);
    }

    static private boolean textCompare(String desc, String search_str) {
        ArrayList<String> desc_arr = stringToArray(desc);
        ArrayList<String> search_arr = stringToArray(search_str);

        for(String search_word: search_arr) {
            for(String desc_word: desc_arr) {
                if(desc_word.equals(search_word)) {
                    return true;
                }
            }
        }

        return false;
        //return (db.indexOf(text) >= 0);
    }

    static private ArrayList<String> stringToArray(String text) {
        boolean last_char_is_latin = false;
        String s = "";
        ArrayList<String> retArr = new ArrayList<>();

        for (int i = 0; i < text.length(); i++) {
            //Log.i(TAG, "stringToArray: text.chars(i)="+text.charAt(i));
            int c = text.codePointAt(i);
            if(c >= 0x4E00) {
                retArr.add(text.substring(i, i+1));
                last_char_is_latin = false;
            } else {
                if(last_char_is_latin) {
                    s = s + text.substring(i, i+1);
                } else {
                    s = s + " " + text.substring(i, i+1);
                }
                last_char_is_latin = true;
            }
        }
        s = s.trim();
        String[] arr = s.split(" ");

        for (int i = 0; i < arr.length; i++) {
            if(arr[i].length() > 0)
                retArr.add(arr[i]);
        }

        return  retArr;
    }

    static public void RetrievingImages(Context context) {
        lstADate = new ArrayList<>() ;
        lstAPhoto = new ArrayList<>();

        final String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID, MediaStore.Images.Media.DATE_TAKEN};
        final String orderBy = MediaStore.Images.Media.DATE_ADDED;
        //final Cursor imgCur = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, orderBy + " DESC");
        //TODO: refresh list after took photo
        context.getContentResolver().notifyChange(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null);
        final Cursor imgCur = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, orderBy + " ASC");

        String header_date = "";
        int index = 0;


        while ( imgCur!=null && imgCur.moveToNext() ) {
            String imagePath = imgCur.getString(imgCur.getColumnIndex(MediaStore.Images.Media.DATA));
            //Log.i(TAG, "RetrievingImages:imagePath=> "+imagePath);
            String dateTaken = getTakenDate(imgCur.getString(imgCur.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN)));

            String fileName = (new File(imagePath)).getName();
            fileName = fileName.substring(0, fileName.lastIndexOf("."));
            String title = "";
            if(fileName.length() > 15 && fileName.substring(14, 15).equals(".")) {
                title = fileName.substring(15);
            }

            String date = dateTaken.split(" ")[0];
            String time = dateTaken.split(" ")[1];

            if( !header_date.equals(date) ) {
                date =  date.substring(0, 8) + ", " + date.substring(8);
                lstADate.add(date);
                header_date = date;
            }

            lstAPhoto.add(new Photo(index, title, imagePath, date, time));

            index = index + 1;
        }

        imgCur.close();
    }

    static public String addImageToGallery(Context context, File filepath) {
        try {
            return MediaStore.Images.Media.insertImage(context.getContentResolver(),
                    filepath.toString(), filepath.getName(), "Image Description");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
//    static public String addImageToGallery(ContentResolver cr, File filepath) {
//        try {
//            return MediaStore.Images.Media.insertImage(cr, filepath.toString(),
//                    filepath.getName(), "Image Description");
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    static private String getTakenDate(String dateTaken) {
        Long timestamp = Long.parseLong(dateTaken);
        Date date = new Date(timestamp);
        SimpleDateFormat jdf = new SimpleDateFormat("yyyyMMddEEEE HH:mm", Locale.US);
        //jdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        return jdf.format(date);
    }
}

