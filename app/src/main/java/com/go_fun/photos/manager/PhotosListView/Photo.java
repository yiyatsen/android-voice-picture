package com.go_fun.photos.manager.PhotosListView;

import android.graphics.Bitmap;

/**
 * Created by Aws on 28/01/2018.
 */

public class Photo {

//    public enum SECTION_TYPE {
//        HEADER_SECTION,
//        ITEM_SECTION
//    }

//    private SECTION_TYPE SectionType;
    private int Index;
    private String Title;
    private String ImagePath;
    private String DateAdded ;
    private String TimeAdded ;
//    private String Description ;
    private Bitmap Thumbnail ;

//    public Photo(String name, String category) {
//            this.Title = name;
//            this.ImagePath = category;
//        }

    public Photo(int itemIndex,String title, String imagePath, String dateAdded, String timeAdded) {
//        SectionType = itemType;
        Index = itemIndex;
        Title = title;
        ImagePath = imagePath;
        DateAdded = dateAdded;
        TimeAdded = timeAdded;
//        Thumbnail = thumbnail;
    }


//    public SECTION_TYPE getSectionType() {
//        return SectionType;
//    }

    public int getInex() {
        return Index;
    }

    public String getTitle() {
        return Title;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public String getDateAdded() {
        return DateAdded;
    }
    public String getTimeAdded() {
        return TimeAdded;
    }

    public Bitmap getThumbnail() {
        return Thumbnail;
    }


    public void setTitle(String title) {
        Title = title;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    public void setDateAdded(String dateAdded) {
        DateAdded = dateAdded;
    }

    public void setTimeAdded(String timeAdded) {
        TimeAdded = timeAdded;
    }

    public void setThumbnail(Bitmap thumbnail) {
        Thumbnail = thumbnail;
    }
}
