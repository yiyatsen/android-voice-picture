package com.go_fun.photos.manager.ORMLite;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


@Entity(tableName = "photo_table")
public class PhotoDbInfo {
//    @PrimaryKey(autoGenerate = true)
//    public int id;

    @PrimaryKey()
    @NonNull
    public String imagePath;

    @ColumnInfo(name = "title")
    public String title;

    @ColumnInfo(name = "addedDate")
    public String addedDate;

    @ColumnInfo(name = "addedTime")
    public String addedTime;
}