package com.go_fun.photos.manager;

import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.FileObserver;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.go_fun.photos.manager.ORMLite.Db;
import com.go_fun.photos.manager.ORMLite.PhotoDbInfo;
import com.go_fun.photos.manager.ORMLite.PhotoInfoDao;
import com.go_fun.photos.manager.ORMLite.PhotoInfoDatabase;
import com.go_fun.photos.manager.PhotosListView.Photo;
import com.go_fun.photos.manager.Util.FileAccess;
import com.go_fun.photos.manager.Util.GalleryUtil;
import com.go_fun.photos.manager.Util.Listeners;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

import static com.go_fun.photos.manager.Util.Listeners.*;

public class PhotoSaveActivity  extends AppCompatActivity {

    private static final String TAG = "Constraints";
    private Listeners.ButtonEffectOnTouchListener buttonEffectOnTouchListener = new Listeners.ButtonEffectOnTouchListener();
    String photoFileName;
    File photoFile;

    EditText edtTitle;
    public ImageView imgPhoto;
    ImageView ivMic;
    ImageView imgBack;
    ImageView imgSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_save);

        //Log.i(TAG, "onCreate: 1");
        ivMic = findViewById(R.id.ivMic);
        edtTitle = findViewById(R.id.edt_title);
        imgPhoto = findViewById(R.id.photo_image);
        imgSave = findViewById(R.id.imgSave);
        imgBack = findViewById(R.id.imgBack);

        ivMic.setOnTouchListener(buttonEffectOnTouchListener);

        Intent intent = getIntent();
        photoFileName = intent.getExtras().getString("PHOTO_FILE");
        photoFile = new File(photoFileName);

        if(photoFile.exists()) {
            //Log.i(TAG, "onCreate: photoFile.exists()");
            Glide.with(this)
                    .load(photoFileName)
                    .thumbnail(1.0f)
                    .into(imgPhoto);
        }

        edtTitle.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP ) {
                    saveAndExit(v);
                    return true;
                }
                return false;
            }

        });
    }

    public void getSpeechInput(View view) {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Listening...");

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 10);
        } else {
            Toast.makeText(this, R.string.device_not_speech_input, Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String text = edtTitle.getText().toString();
                    int sel_start = edtTitle.getSelectionStart();
                    int sel_end = edtTitle.getSelectionEnd();
                    String speak_text = result.get(0);

                    edtTitle.setText(text.substring(0, sel_start) + speak_text + text.substring(sel_end, text.length()));
                    edtTitle.setSelection(edtTitle.length());
                }
                break;
        }
    }

    public void onClickBack(View view) {
        photoFile.delete();
        startActivity(new Intent(this, CameraActivity.class));
        finish();
    }

    public void onClickSave(View view) {
        saveAndExit(view);
    }

    @NonNull
    private String getString() {
        String editText = edtTitle.getText().toString()
                .replace("*", "").replace(".", "")
                .replace("\\", "").replace("/", "")
                .replace("[", "").replace("]", "")
                .replace(":", "").replace(";", "")
                .replace("|", "").replace("=", "")
                .replace(",", "").trim();
        while (editText.indexOf("  ") >= 0) {
            editText = editText.replace("  ", " ");
        }
        return editText;
    }

    private String insertImageIntoGallery() {
        Bitmap bitmap = BitmapFactory.decodeFile(photoFileName);

        return GalleryUtil.insertImageIntoGallery(getContentResolver(), bitmap, "voice", "picture");
    }

    private void saveAndExit(View view) {
        MediaScannerConnection
            .scanFile(this,
                new String[] { photoFileName }, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    @Override
                    public void onScanCompleted(String path, Uri uri) {
                    Log.i("ExternalStorage", "Scanned " + path + ":");
                    Log.i("ExternalStorage", "-> uri=" + uri);
                }
                });
        final View v = view;
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                savePhotoDescThread(v);
            }
        });
        t.start();
        try { t.join(); } catch (InterruptedException e) { e.printStackTrace(); }

//        Intent resultIntent = new Intent();
//        resultIntent.putExtra("REFRESH_LIST", true);
//        setResult(Activity.RESULT_OK, resultIntent);

        startActivity(new Intent(this, CameraActivity.class));
        finish();
    }

    public void savePhotoDescThread(View view) {
        PhotoInfoDatabase photoInfoDatabase = Room.databaseBuilder(this, PhotoInfoDatabase.class, "photos").build();
        PhotoInfoDao dao = photoInfoDatabase.getPhotoInfoDao();

        PhotoDbInfo photoInfo;
        if(Db.photoInfoInDb(photoFile.getAbsolutePath())) {
            for (int i = 0; i < Db.lstPhotoInfo.size(); i++) {
                if (Db.lstPhotoInfo.get(i).imagePath.equals(photoFile.getAbsolutePath())) {
                    photoInfo = Db.lstPhotoInfo.get(i);
                    photoInfo.title = edtTitle.getText().toString();
                    Db.lstPhotoInfo.set(i, photoInfo);
                    break;
                }
            }
        } else {
            PhotoDbInfo p = new PhotoDbInfo();
            p.title = edtTitle.getText().toString();
            p.imagePath = photoFileName;
            p.addedDate = "";
            p.addedTime = "";
            Db.lstPhotoInfo.add(p);
        }

        Photo p = new Photo(FileAccess.lstAPhoto.size(), edtTitle.getText().toString(), photoFileName, "", "");
        for (int i=0; i < FileAccess.lstSPhoto.size(); i++) {
            Photo pa = FileAccess.lstSPhoto.get(i);
            if(pa.getImagePath().equals(photoFileName)) {
                pa.setTitle(edtTitle.getText().toString());
                FileAccess.lstSPhoto.set(i, pa);
                break;
            }
        }
        FileAccess.lstAPhoto.add(p);

        Db.add(dao, photoFileName, edtTitle.getText().toString(), "", "");
    }

}

//class SDCardFileObserver extends FileObserver {
//
//    public SDCardFileObserver(String path) {
//        super(path);
//    }
//
//    @Override
//    public void onEvent(int event, @Nullable String path) {
//
//    }
//}