package com.go_fun.photos.manager.PhotosListView;

import android.app.Activity;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.go_fun.photos.manager.ORMLite.Db;
import com.go_fun.photos.manager.ORMLite.PhotoDbInfo;
import com.go_fun.photos.manager.ORMLite.PhotoInfoDao;
import com.go_fun.photos.manager.ORMLite.PhotoInfoDatabase;
import com.go_fun.photos.manager.R;
import com.go_fun.photos.manager.Util.FileAccess;
import com.go_fun.photos.manager.Util.Listeners;

import java.util.ArrayList;
import java.util.Locale;

public class SinglePhotoActivity extends AppCompatActivity {

    private static final String TAG = "SinglePhotoActivity";
    private Listeners.ButtonEffectOnTouchListener buttonEffectOnTouchListener = new Listeners.ButtonEffectOnTouchListener();
    String mPhotoFile;
    //File photoFile;
    int image_index;
    Photo photo;
    EditText edtTitle;
    ImageView imgPhoto;
    ImageView ivMic;
    ImageView imgBack;
    ImageView imgSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_photo);

        ivMic = findViewById(R.id.ivMic);
        edtTitle = findViewById(R.id.edt_title);
        imgPhoto = findViewById(R.id.photo_image);
        imgSave = findViewById(R.id.imgSave);
        imgBack = findViewById(R.id.imgBack);

        ivMic.setOnTouchListener(buttonEffectOnTouchListener);
        imgBack.setOnTouchListener(buttonEffectOnTouchListener);
        imgSave.setOnTouchListener(buttonEffectOnTouchListener);

        Intent intent = getIntent();
        image_index = intent.getExtras().getInt("IMAGE_INDEX");
        photo = FileAccess.lstAPhoto.get(image_index);
        mPhotoFile = photo.getImagePath();
        //photoFile = new File(mPhotoFile);

        Glide.with(this)
                .load(mPhotoFile)
                .thumbnail(1.0f)
                .into(imgPhoto);

        edtTitle.setText(photo.getTitle());
        edtTitle.setSelection(0, edtTitle.length());

        edtTitle.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_UP ) {
                    //Log.i(TAG, "onKey: ");
                    saveAndExit(v);
                    return true;
                }
                return false;
            }

        });
    }

    public void getSpeechInput(View view) {

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Listening...");

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 10);
        } else {
            Toast.makeText(this, R.string.device_not_speech_input, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String text = edtTitle.getText().toString();
                    int sel_start = edtTitle.getSelectionStart();
                    int sel_end = edtTitle.getSelectionEnd();
                    String speak_text = result.get(0);

                    edtTitle.setText(text.substring(0, sel_start) + speak_text + text.substring(sel_end, text.length()));
                    edtTitle.setSelection(edtTitle.length());
                }
                break;
        }
    }

    public void onClickBack(View view) {
        finish();
    }

    public void onClickSave(View view) {
        saveAndExit(view);
    }

    private void saveAndExit(View view) {
        final View v = view;
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                savePhotoDescThread(v);
            }
        });
        t.start();
        try { t.join(); } catch (InterruptedException e) { e.printStackTrace(); }

        Intent resultIntent = new Intent();
        resultIntent.putExtra("REFRESH_LIST", true);
        setResult(Activity.RESULT_OK, resultIntent);

        finish();
    }

    public void savePhotoDescThread(View view) {
        PhotoInfoDatabase photoInfoDatabase = Room.databaseBuilder(this, PhotoInfoDatabase.class, "photos").build();
        PhotoInfoDao dao = photoInfoDatabase.getPhotoInfoDao();

        PhotoDbInfo photoInfo;
        if(Db.photoInfoInDb(photo.getImagePath())) {
            for (int i = 0; i < Db.lstPhotoInfo.size(); i++) {
                if (Db.lstPhotoInfo.get(i).imagePath.equals(photo.getImagePath())) {
                    photoInfo = Db.lstPhotoInfo.get(i);
                    photoInfo.title = edtTitle.getText().toString();
                    Db.lstPhotoInfo.set(i, photoInfo);
                    break;
                }
            }
        } else {
            PhotoDbInfo p = new PhotoDbInfo();
            p.title = edtTitle.getText().toString();
            p.imagePath = photo.getImagePath();
            p.addedDate = photo.getDateAdded();
            p.addedTime = photo.getTimeAdded();
            Db.lstPhotoInfo.add(p);
        }

        Photo p = new Photo(image_index, edtTitle.getText().toString(), photo.getImagePath(), photo.getDateAdded(), photo.getTimeAdded());
        for (int i=0; i < FileAccess.lstSPhoto.size(); i++) {
            Photo pa = FileAccess.lstSPhoto.get(i);
            if(pa.getImagePath().equals(photo.getImagePath())) {
                pa.setTitle(edtTitle.getText().toString());
                FileAccess.lstSPhoto.set(i, pa);
                break;
            }
        }
        FileAccess.lstAPhoto.add(image_index, p);

        Db.add(dao, photo.getImagePath(), edtTitle.getText().toString(), photo.getDateAdded(), photo.getTimeAdded());
    }
}
