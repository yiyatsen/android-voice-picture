package com.go_fun.photos.manager.ORMLite;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface PhotoInfoDao {
    /**
     * 查询
     *
     * @return
     */
    @Query("SELECT * FROM photo_table")
    public List<PhotoDbInfo> getAllPhotoInfo();

    /**
     * 添加
     *
     * @param photos
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertPhotoInfo(PhotoDbInfo... photos);

    /**
     * 更新
     *
     * @param photos
     */
    @Update
    public void updatePhotoInfo(PhotoDbInfo... photos);

    /**
     * 删除
     *
     * @param photos
     */
    @Delete
    public void deletePhotoInfo(PhotoDbInfo... photos);
}