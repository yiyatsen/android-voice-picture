package com.go_fun.photos.manager;


public interface CameraInterface {
    void openPhotoFileSave(String photoFile);
    void goGallery();
}